from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.responses import HTMLResponse


app = FastAPI()


@app.get("/")
def homepage():
    with open("index.html") as f:
        return HTMLResponse(f.read())

# creates an empty list of active connections from many WebSockets.
connections = []

# tells FastAPI that the following function will handle WebSocket connections at the path "/ws", which is what we put in the HTML.
@app.websocket("/ws")

# declares the method to handle the WebSocket connection. The parameter is of type WebSocket, which means that FastAPI will give you that kind of object when the function gets called. This is similar to when we have request: Request in our parameters to get a Request object. But WebSockets are not request-response. They are persistent communication channels.
async def websocket_endpoint(websocket: WebSocket):

    # waits for a connection from a client like Google Chrome.
    await websocket.accept()

    # adds the new WebSocket object to the list of active connections.
    connections.append(websocket)

    # sets up an exception handler for when a WebSocket disconnects.
    try:

        # sets up an infinite loop so that the function can continue to read new messages from the browser.
        while True:

            # gets the text sent through the WebSocket.
            data = await websocket.receive_text()

            # loops through all of the active connections.
            for connection in connections:

                # sends that data to that connection, effectively sending the data to all connections in the loop.
                await connection.send_text(data)

    # catches the exception raised when a WebSocket disconnects and is no longer active.
    except WebSocketDisconnect:

        # removes the WebSocket from the list of active connections.
        connections.remove(websocket)
